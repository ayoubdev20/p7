
const path = require('path');
const express = require('express');
const ejs = require('ejs');
const bodyParser = require('body-parser');
const app = express();



 



app.use(express.static(path.join(__dirname, 'assets')));
app.set('views',path.join(__dirname, 'views'));
			
app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/',(req, res) => {
         res.render('NarutoGame');
    });

app.listen(3001, () => {
    console.log('Server is running at port 3001');
});
